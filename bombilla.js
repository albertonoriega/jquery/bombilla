$(function () {
    //Cuando le hacemos hover
    $('img').hover(function () {

        $('.bombilla').css('visibility', 'hidden');

        //OTRA FORMA (usando una clase creada en CSS)
        //$('.bombilla').addClass("encendida")


    });
    // Cuando el ratón deja de estar enima de la imágen
    $('img').mouseover(function () {

        $('.bombilla').css('visibility', 'visible');

        //OTRA FORMA (usando una clase creada en CSS)
        //$('.bombilla').removeClass("encendida")
    });
});
